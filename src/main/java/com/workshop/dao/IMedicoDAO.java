package com.workshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.workshop.models.Medico;
@Repository
public interface IMedicoDAO extends JpaRepository<Medico, Integer>{

}
