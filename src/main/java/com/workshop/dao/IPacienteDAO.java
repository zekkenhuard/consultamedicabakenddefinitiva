package com.workshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.workshop.models.Paciente;
@Repository
public interface IPacienteDAO extends JpaRepository<Paciente, Integer>{

}
