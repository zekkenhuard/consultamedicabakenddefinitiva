package com.workshop.service;

import java.util.List;



import com.workshop.models.Menu;

public interface IMenuService{
	
	List<Menu> listarMenuPorUsuario(String nombre);
	public Menu registrar(Menu menu);
	public Menu modificar(Menu menu);
	public void eliminar(int idMenu);
	public Menu listarId(int idMenu);
	public List<Menu> listar();
}
