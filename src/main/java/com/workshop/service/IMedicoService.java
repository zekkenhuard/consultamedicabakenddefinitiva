package com.workshop.service;

import java.util.List;

import com.workshop.models.Medico;

public interface IMedicoService {
	Medico persist(Medico e);
	List<Medico> getAll();
	Medico fingById(Integer id);
	Medico	merge(Medico e);
	void delete(Integer id);
}
